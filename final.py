'''requirements :
pip install beautifulsoup4
pip install requests
pip install lxml
pip install selenium
pip install webdriver_manager
'''
​
from bs4 import BeautifulSoup as bs
import requests
from selenium import webdriver
import pandas as pd
from webdriver_manager.chrome import ChromeDriverManager
from flask import Flask,jsonify, request
import numpy as np
​
app = Flask(__name__)
​
#driver = webdriver.Chrome(ChromeDriverManager().install())
​
@app.route('/', methods=['GET','POST'])
def scrap():
    #Part 1 : web scrapping 
    driver=webdriver.Chrome(r"C:\Users\Client\Desktop\final_web_scrapping\chromedriver.exe")
    products=[] 
    prices=[] 
    urls = ["https://www.jumia.com.tn/mlp-anniversaire/", "https://www.jumia.com.tn/pc-portables/", "https://www.jumia.com.tn/manteaux-hommes/"]
    for url in urls :
        file1= url[:-1]
        list_f= file1.split('/')
        filename= list_f[-1]
        print('------------------------------')
        print('filename is = ',filename)
        print('url is = ',url)
        print('------------------------------')
        driver.get(url)
        content = driver.page_source
        #print('content',content)
        soup = bs(content)
        for a in soup.findAll(attrs={'class':'info'}):
            name=a.find(attrs={'class':'name'})
            price=a.find(attrs={'class':'prc'})
            #rating=a.find('div', attrs={'class':''})
            products.append(name.text)
            prices.append(price.text)
            #ratings.append(rating.text)
            print('------------------------------')
            print('the article named : '+str(name)+' got the price : '+str(price)+' for the products'+str(products))
            print('\n ----***--- \n')
        #part 2 : save the extracted datab in csv file
        df = pd.DataFrame({'Product Name':products,'Price':prices})
        df.to_csv('C:/Users/Client/Desktop/final_web_scrapping/'+filename+'.csv', index=False, encoding='utf-8')
        print('----------++++++++++-----------++++++++++++----------')
        df.to_json('C:/Users/Client/Desktop/final_web_scrapping/'+filename+'.json')
        
    return'Done'
​
​