
'''requirements :
pip install beautifulsoup4
pip install requests
pip install lxml
pip install selenium
pip install webdriver_manager
pip install flask_mysqldb
'''

#from tkinter.ttk import Separator
from bs4 import BeautifulSoup as bs
import requests
import chromedriver_binary
from selenium import webdriver
import pandas as pd
#from soupsieve import select
from webdriver_manager.chrome import ChromeDriverManager
from flask import Flask,jsonify, request
import numpy as np
   
from flask_mysqldb import MySQL
app = Flask(__name__)
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'test'
mysql = MySQL(app)
#driver = webdriver.Chrome(ChromeDriverManager().install())
@app.route('/', methods=['GET','POST'])
def liste():

    #Part 1 : web scrapping 
    driver=webdriver.Chrome(r"C:\Users\Marwen Ben Salem\Desktop\neww\chromedriver.exe")
    
    products=[] 
    prices=[] 
    urls = [
             "https://www.jumia.com.tn/pc-portables/",
             "https://www.jumia.com.tn/produit-lessive/",
             "https://www.jumia.com.tn/epicerie-alimentaire/",
             "https://www.jumia.com.tn/produits-epicerie-boissons/",
             "https://www.jumia.com.tn/maquillage/",
             "https://www.jumia.com.tn/sante-beaute-beaute-soins-personnels/",
             "https://www.jumia.com.tn/sante-beaute-parfums/",
             "https://www.jumia.com.tn/telephones-smartphones/",
             "https://www.jumia.com.tn/tablettes-tactiles/",
             "https://www.jumia.com.tn/accessoires-telephone/",
             "https://www.jumia.com.tn/robots-cuiseurs/",
             "https://www.jumia.com.tn/cuisine-salle-manger-ustensiles-cuisine-gadgets/",
             "https://www.jumia.com.tn/fashion-mode-homme/",
             "https://www.jumia.com.tn/chaussures-hommes/",
             "https://www.jumia.com.tn/accessoires-hommes-mode/",
             "https://www.jumia.com.tn/fashion-mode-femme/",
             "https://www.jumia.com.tn/peripheriques-logiciels-accessoires/",
             "https://www.jumia.com.tn/imprimantes-pc/",
             "https://www.jumia.com.tn/stockage/",
             "https://www.jumia.com.tn/tvs/",
             "https://www.jumia.com.tn/camera-video/",
             "https://www.jumia.com.tn/connectiques/",
             "https://www.jumia.com.tn/robots-cuiseurs/",
             "https://www.jumia.com.tn/cuisine-cuisson/",
             "https://www.jumia.com.tn/jeux-videos-consoles/",
             "https://www.jumia.com.tn/tablettes-tactiles/",
             "https://www.jumia.com.tn/fashion-mode-homme/",
             "https://www.jumia.com.tn/fashion-mode-femme/",
             "https://www.jumia.com.tn/ordinateurs-pc/"

            ]

    for url in urls :
        file1= url[:-1]
        list_f= file1.split('/')
        filename= list_f[-1]
        print('------------------------------')
        print('filename is = ',filename)
        print('url is = ',url)
        print('------------------------------')
        driver.get(url)
        content = driver.page_source
        #print('content',content)
        soup = bs(content)
        for a in soup.findAll(attrs={'class':'info'}):
            name=a.find(attrs={'class':'name'})
            price=a.find(attrs={'class':'old'})
            if(price == None):
                price=a.find(attrs={'class':'prc'})
            
            #rating=a.find('div', attrs={'class':''})
            #print(name)
            myproduct_name = (str(name)).replace('"','')
            myproduct_name = myproduct_name.replace('<h3 class=name>','')
            myproduct_name = myproduct_name.replace('</h3>','')
            myproduct_name = myproduct_name.replace(',','')
            
            
            #.replace('"','')).replace(',',''))
            myproduct_price = (str(price)).replace('"','')  
            myproduct_price = (str(price)).replace(',','')
            myproduct_price = myproduct_price.replace('<div class="old">','')
            myproduct_price = myproduct_price.replace('<div class="prc">','')
            myproduct_price = myproduct_price.replace('</div>','')
            myproduct_price = myproduct_price.replace(' TND','')
            
            
            if  myproduct_name != '':
                products.append(myproduct_name)
                prices.append(myproduct_price)
                print('price : '+ myproduct_price+' pour produit : '+ myproduct_name)
            
            #ratings.append(rating.text)
            #print('------------------------------')
           # print('the article named : '+str(name)+' got the price : '+str(price)+' for the products'+str(products))
            #print('\n ----***--- \n')
        #part 2 : save the extracted datab in csv file
        df = pd.DataFrame({'Product Name':products,'Price':prices})
        df.to_csv('C:/Users/Marwen Ben Salem/Desktop/neww/'+filename+'.csv', index=False, encoding='utf-8')
        print('----------++++++++++-----------++++++++++++----------')
        df.to_json('C:/Users/Marwen Ben Salem/Desktop/final_web_scrapping/'+filename+'.json')
        
    return'Done'




@app.route('/insert', methods=['GET','POST'])
def insert():
   
    if request.method == 'POST' or request.method == 'GET':    
        subcategorie = request.args['Categorie']
        if subcategorie == 'Téléphone Portable':
            pdt = pd.read_csv(r'C:\Users\Marwen Ben Salem\Desktop\neww\telephones-smartphones.csv' , sep = ',')
        if subcategorie =='Accessoires téléphone':
            pdt = pd.read_csv(r'C:\Users\Marwen Ben Salem\Desktop\neww\accessoires-telephone.csv' , sep = ',')
        if subcategorie =='Cuisine':
            pdt = pd.read_csv(r'C:\Users\Marwen Ben Salem\Desktop\neww\cuisine-salle-manger-ustensiles-cuisine-gadgets.csv' , sep = ',')
        if subcategorie =='epicerie-alimentaire':
            pdt = pd.read_csv(r'C:\Users\Marwen Ben Salem\Desktop\neww\epicerie-alimentaire.csv' , sep = ',')
        if subcategorie =='Maquillage':
            pdt = pd.read_csv(r'C:\Users\Marwen Ben Salem\Desktop\neww\maquillage.csv' , sep = ',')
        if subcategorie =='Lessive':
            pdt = pd.read_csv(r'C:\Users\Marwen Ben Salem\Desktop\neww\produit-lessive.csv' , sep = ',')
        if subcategorie =='Boissons':
            pdt = pd.read_csv(r'C:\Users\Marwen Ben Salem\Desktop\neww\produits-epicerie-boissons.csv' , sep = ',')
        if subcategorie =='Eléctromenager':
            pdt = pd.read_csv(r'C:\Users\Marwen Ben Salem\Desktop\neww\robots-cuiseurs.csv' , sep = ',')
        if subcategorie =='Beauté & soin personnel':
            pdt = pd.read_csv(r'C:\Users\Marwen Ben Salem\Desktop\neww\sante-beaute-beaute-soins-personnels.csv' , sep = ',')
        if subcategorie =='Parfum':
            pdt = pd.read_csv(r'C:\Users\Marwen Ben Salem\Desktop\neww\sante-beaute-parfums.csv' , sep = ',')
        if subcategorie =='Mode femme':
            pdt = pd.read_csv(r'C:\Users\Marwen Ben Salem\Desktop\neww\fashion-mode-femme.csv' , sep = ',')
        if subcategorie =='Mode homme':
            pdt = pd.read_csv(r'C:\Users\Marwen Ben Salem\Desktop\neww\fashion-mode-homme.csv' , sep = ',')
        if subcategorie =='Ordinateur':
            pdt = pd.read_csv(r'C:\Users\Marwen Ben Salem\Desktop\neww\ordinateurs-pc.csv' , sep = ',')
        if subcategorie =='Imprimante':
            pdt = pd.read_csv(r'C:\Users\Marwen Ben Salem\Desktop\neww\imprimantes-pc.csv' , sep = ',')
        if subcategorie =='Stockage de donnée':
            pdt = pd.read_csv(r'C:\Users\Marwen Ben Salem\Desktop\neww\stockage.csv' , sep = ',')
        if subcategorie =='Accessoires et pérépheriques':
            pdt = pd.read_csv(r'C:\Users\Marwen Ben Salem\Desktop\neww\peripheriques-logiciels-accessoires.csv' , sep = ',')
        if subcategorie =='Télévisions':
            pdt = pd.read_csv(r'C:\Users\Marwen Ben Salem\Desktop\neww\tvs.csv' , sep = ',')


    df= pdt
    #pd.read_csv(r'C:\Users\Marwen Ben Salem\Desktop\neww\products.csv' , sep = ',')
    #df.to_json(r'C:\Users\Marwen Ben Salem\Desktop\neww\products.json')
    lst= [] 
    liste= []
    list_avec_mots = []
    mot = ''
    somme = 0
    
   
    if request.method == 'POST' or request.method == 'GET':
        #price= request.args['price']
        name = request.args['name']
        longeur_de_name = len(name)
        print('le longeur du name est egale : ', longeur_de_name)
        for i in range (len(name)):
            if name[i] != ' ':
                mot +=name[i]
            if name[i] == ' ' or i+1 == longeur_de_name:
                list_avec_mots.append(mot)
                mot = ''
                name.replace(mot+' ','')
        #print('la liste avec les mots separer est ici : ',list_avec_mots)
        print('----------------> Recherche du produit -- ', name.upper() , ' -- <----------------')
        print('*********************************************************************************************************************')
        new_df_price = df['Price']
        new_df_name = df['Product Name']
        lst = np.array(new_df_price)
        
        k = 0
        verif = ''
        for i in range(len(lst)): 
            
            
            original= (new_df_price[i])
            compteur = 0
            
            for j in range(len(list_avec_mots)):
                if list_avec_mots[j].upper() in new_df_name[i].upper():
                    compteur += 1
                    verif = 'trouver'
                if compteur >= ((len(list_avec_mots)*0.8)) and verif =='trouver' :

                    
                     print('ce produit du nom <',new_df_name[i],'> est trouver avec le prix : <',new_df_price[i],'>.')
                     somme += float(new_df_price[i])
                     k += 1
                #else:
                 #   verif = 'pas suffisant trouver'
        message = '0'
        if verif == 'trouver' and somme != 0:  
            moyenne = somme//k
            print('*********************************************************************************************************************')
            print('---------------->',k,' produits trouve avec un moyen de prix egale a : ', moyenne,' <----------------')
            #print('ici cest price :',price)
            #pr = float(price)
            
            #promo = moyenne*0.1
            
            #maximum = moyenne+promo 
            #minimum = moyenne-promo
            message = str(moyenne)
        #     if pr <= maximum and pr >= minimum  :
        #         print('le prix proposé est  : '+str(round(pr))+'  avec une moyenne de promo  est  '+str(round(moyenne)*0.1))
        #         print('la valeur maximale est'+str(round(moyenne)+promo)+' avec une valeur minimale de  '+str(round(moyenne-promo)))
        #         cur = mysql.connection.cursor()
        #         query= "INSERT INTO promo (name, price) VALUES (%s, %s)"
        #         cur.execute(query,(name,price))
        #         mysql.connection.commit()b
        #         message='added successfully '
                   
        #     else : 
        #         message= 'try another price please'+str(somme//k)
        # else :
        #     message='Produit n exisite pas dans notre analyse sur les autres sites'
        #     verif = ''
    # cur.close()
    return message




if(__name__ == '__main__'):
    app.run(debug = True)


